﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QUODA.ELearning.API.Models
{
    public class InputDTO
    {
        public string Codesnippet { get; set; }
    }
}
