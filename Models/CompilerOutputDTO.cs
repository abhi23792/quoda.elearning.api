﻿using System.Collections.Generic;

namespace QUODA.ELearning.API.Models
{
    public class CompilerOutputDTO
    {
        public List<string> Output { get; set; }

        public List<string> ErrorAndWarnings { get; set; }
    }
}
