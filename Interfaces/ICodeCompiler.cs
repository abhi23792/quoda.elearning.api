﻿using QUODA.ELearning.API.Models;

namespace QUODA.ELearning.API.Interfaces
{
    public interface ICodeCompiler
    {
        /// <summary>
        /// Runs the specific compiler for the language based on Language(R, Python).
        /// </summary>
        /// <param name="tempFileArguments">Temporary File Path.</param>
        /// <param name="compilerFilePath">Executable file path for the compiler.</param>
        void RunShellCommand(string tempFileArguments, string compilerFilePath);

        /// <summary>
        /// Compiles the code sent from the UI and returns the output along with Errors and warnings.
        /// </summary>
        /// <param name="codeSnippet">Code string.</param>
        /// <returns></returns>
        CompilerOutputDTO CompileCode(string codeSnippet);
    }
}
