﻿using QUODA.ELearning.API.Interfaces;
using QUODA.ELearning.API.Models;
using QUODA.ELearning.API.Utilities;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;

namespace QUODA.ELearning.API.Managers
{
    public class CodeCompileManager : ICodeCompiler
    {
        private CompilerOutputDTO _outputDTO;
        private static string compilerFilePath = @"D:\ProgramsFiles\Python\Python38\python.exe";

        public void RunShellCommand(string tempFileArguments, string compilerFilePath)
        {
            ProcessStartInfo cmdStartInfo = new ProcessStartInfo();
            cmdStartInfo.FileName = compilerFilePath;
            cmdStartInfo.Arguments = tempFileArguments;
            cmdStartInfo.RedirectStandardOutput = true;
            cmdStartInfo.RedirectStandardError = true;
            cmdStartInfo.RedirectStandardInput = true;
            cmdStartInfo.UseShellExecute = false;
            cmdStartInfo.CreateNoWindow = true;

            Process cmdProcess = new Process();
            cmdProcess.StartInfo = cmdStartInfo;
            cmdProcess.ErrorDataReceived += cmd_Error;
            cmdProcess.OutputDataReceived += cmd_DataReceived;
            cmdProcess.EnableRaisingEvents = true;
            cmdProcess.Start();
            cmdProcess.BeginOutputReadLine();
            cmdProcess.BeginErrorReadLine();

            cmdProcess.WaitForExit();
        }

        private void cmd_DataReceived(object sender, DataReceivedEventArgs e)
        {
            _outputDTO.Output.Add(e.Data);
        }

        private void cmd_Error(object sender, DataReceivedEventArgs e)
        {
            _outputDTO.ErrorAndWarnings.Add(e.Data);
        }

        public CompilerOutputDTO CompileCode(string codeSnippet)
        {
            _outputDTO = new CompilerOutputDTO
            {
                ErrorAndWarnings = new List<string>(),
                Output = new List<string>()
            };

            var stream = ReadWriteOperations.GenerateStreamFromString(codeSnippet);
            var fileName = Path.GetTempFileName();

            try
            {
                using (FileStream fs = File.OpenWrite(fileName))
                {
                    stream.CopyTo(fs);
                }

                this.RunShellCommand(fileName, compilerFilePath);

            }
            finally
            {
                File.Delete(fileName);
            }

            return _outputDTO;
        }
    }
}
