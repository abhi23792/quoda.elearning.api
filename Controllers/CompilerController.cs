﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using QUODA.ELearning.API.Interfaces;
using QUODA.ELearning.API.Models;

namespace QUODA.ELearning.API.Controllers
{
    [AllowAnonymous]
    [ApiController]
    [Route("api/[controller]/[action]")]
    public class CompilerController : ControllerBase
    {
        private readonly ILogger<CompilerController> _logger;
        private readonly ICodeCompiler _codeCompiler;

        public CompilerController(ILogger<CompilerController> logger,
                                  ICodeCompiler codeCompiler)
        {
            _logger = logger;
            _codeCompiler = codeCompiler;
        }

        [HttpGet]
        public List<string> Get()
        {
            return new List<string>
            {
                "Compiler API Get method called."
            };
        }

        [HttpPost]
        public async Task<ActionResult<CompilerOutputDTO>> Compile([FromBody] InputDTO codeSnippet)
        {
            var output = new CompilerOutputDTO();

            await Task.Run(() => output = GetCompilerOutput(codeSnippet.Codesnippet));

            return Ok(output);
        }

        private CompilerOutputDTO GetCompilerOutput(string codeSnippet)
        {
            return _codeCompiler.CompileCode(codeSnippet);
        }
    }
}